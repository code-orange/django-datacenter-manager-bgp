from django_mdat_customer.django_mdat_customer.models import *


class DcBgpIp4(models.Model):
    peer = models.ForeignKey("DcBgpPeer", models.DO_NOTHING, db_column="peer")
    ixp = models.ForeignKey("DcBgpIxp", models.DO_NOTHING, db_column="ixp")
    ip = models.GenericIPAddressField(protocol="IPv4", unique=True)

    def __str__(self):
        return "AS" + str(self.peer.asn) + " @ " + self.ixp.name + " via " + self.ip

    class Meta:
        db_table = "dc_bgp_ip4"


class DcBgpIp6(models.Model):
    peer = models.ForeignKey("DcBgpPeer", models.DO_NOTHING, db_column="peer")
    ixp = models.ForeignKey("DcBgpIxp", models.DO_NOTHING, db_column="ixp")
    ip = models.GenericIPAddressField(protocol="IPv6", unique=True)

    def __str__(self):
        return "AS" + str(self.peer.asn) + " @ " + self.ixp.name + " via " + self.ip

    class Meta:
        db_table = "dc_bgp_ip6"


class DcBgpIxp(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "dc_bgp_ixp"


class DcBgpPeer(models.Model):
    customer = models.ForeignKey(MdatCustomers, models.DO_NOTHING, db_column="customer")
    asn = models.IntegerField(unique=True)

    def __str__(self):
        return "AS" + str(self.asn) + " - " + self.customer.name

    class Meta:
        db_table = "dc_bgp_peer"
